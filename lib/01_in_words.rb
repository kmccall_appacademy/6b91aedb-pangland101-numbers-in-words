require 'byebug'

class Fixnum
  def in_words
    singletons, two_digit_weirdos, _scale = load_hashes

    return singletons[self] if singletons[self]
    return two_digit_weirdos[self] if two_digit_weirdos[self]

    number = self
    converter(number) # this handles non-trivial cases
  end

  private

  def converter(num)
    result = ""

    num_length = num.to_s.length
    use_scale = true # says whether to print million, hundred, etc.

    until num_length == 0
      lead_term = num / (10**(num_length - 1))

      case num_length % 3 # handle by hundreth, tenth, or one palce
      when 0
        num, use_scale, addon = handle0(lead_term, num, use_scale)
      when 1
        num, use_scale, addon = handle1(lead_term, num, use_scale)
      when 2
        num, use_scale, addon = handle2(lead_term, num, use_scale)
      end

      result += addon
      num_length == 1 ? num_length = 0 : num_length = num.to_s.length
    end

    result[0...-1]
  end

  def handle0(lead_term, num, use_scale)
    singletons, _two_dig, _scale = load_hashes
    num_length = num.to_s.length
    addon = ""

    if lead_term != 0
      addon += "#{singletons[lead_term]} hundred "
      use_scale = true
    end

    addon += failsafe0(lead_term, num, use_scale)
    num = num - lead_term * 10**(num_length - 1)

    [num, use_scale, addon]
  end

  def handle1(lead_term, num, use_scale)
    singletons, _two_dig, scale = load_hashes
    num_length = num.to_s.length
    addon = ""

    addon += "#{singletons[lead_term]} " if lead_term != 0
    if use_scale && num_length > 1
      addon += "#{scale[num_length]} "
    end
    num = num - lead_term * 10**(num_length - 1)

    use_scale = false
    [num, use_scale, addon]
  end

  def handle2(lead_term, num, use_scale)
    _singletons, two_dig, scale = load_hashes
    num_length = num.to_s.length
    double_term = num.to_s[0..1].to_i # first two digits
    addon = ""

    if two_dig[double_term]
      addon += two_dig[double_term] + " "
      num = num - double_term * 10**(num_length - 2)
      addon += "#{scale[num_length - 1]} " if num_length > 3
      use_scale = false
    else
      addon += "#{two_dig[lead_term * 10]} "
      use_scale = true
      num = num - lead_term * 10**(num_length - 1)
    end

    [num, use_scale, addon]
  end

  def failsafe0(lead_term, num, use_scale)
    _singletons, _two_dig, scale = load_hashes
    num_b4 = num
    num = num - lead_term * 10**(num.to_s.length - 1)
    addon = ""

    if num_b4 >= num * 1000 && use_scale
      (num.to_s.length..num_b4.to_s.length).each do |el|
        addon = "#{scale[el]} " if scale[el]
      end
    end

    addon
  end

  def load_hashes
    singletons = { 0 => "zero", 1 => "one", 2 => "two",
    3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven",
    8 => "eight", 9 => "nine" }

    two_digit_weirdos = { 10 => "ten", 11 => "eleven", 12 => "twelve",
    13 => "thirteen", 14 => "fourteen", 15 => "fifteen",
    16 => "sixteen",17 => "seventeen", 18 => "eighteen",
    19 => "nineteen", 20 => "twenty", 30 => "thirty", 40 => "forty",
    50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
    90 => "ninety" }

    scale = { 4 => "thousand", 7 => "million", 10 => "billion",
    13 => "trillion" }

    [singletons, two_digit_weirdos, scale]
  end
end
